#!/usr/bin/ruby
# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# frozen_string_literal: true

require 'erb'
require 'fileutils'
require 'json'
require 'open-uri'

include ERB::Util

COMMIT_REF_NAME = ARGV[0]
URL = ARGV[1] || 'https://registry.hub.docker.com/v1/repositories/USERNAME/PROJECT/tags'
LINK_FORMAT = ARGV[2] || 'https://gitlab.example.com/GROUP/PROJECT/-/raw/V%s/README-sources.md'

prefix = 'V'

Output_d = '.lint-changes/out'

def info(text)
  #HACK: not to stdout (because stdout is part of the API!):
  $stderr.puts text if $VERBOSE # rubocop:disable Style/StderrPuts
end

versions = []

## - Add new version's tag name if really a version:
new_version_text = COMMIT_REF_NAME.delete_prefix(prefix)
if new_version_text != COMMIT_REF_NAME
  new_version = Gem::Version.new(new_version_text)
  warn new_version
  versions.push new_version
end

o = JSON.parse(URI(URL).read)

warn JSON.pretty_generate o

## - Collect all past versions' tag names:
o.each do |t|
  name = t['name']
  versions.push Gem::Version.new(name) if name =~ /\A[0-9]+/
  #NOTE: also detect versions if run against the private Docker registry,
  # which prefixes version tag names:
  n = prefix.length
  versions.push Gem::Version.new(name.slice(n, name.length - n)) if name =~ /\AV[0-9]+/
end

version_groups = []

## - Compute concrete version tags and their aliases:
approx_versions = []
i = 0
previous_v = ''
versions.sort.uniq.reverse.each do |v|
  warn "V? #{previous_v} #{v} #{previous_v.to_s.start_with?(v.to_s)} #{previous_v <=> v}"
  if previous_v.to_s.start_with?(v.to_s)
    previous_v = v
    next
  end

  #HACK: [...].x.0 <=> [...].x => the sort order is not deterministic for those
  # => explicit guard needed:
  next if (previous_v <=> v) == 0

  i += 1
  previous_v = v
  print v
  s = v.to_s
  versions = [v]
  while (n = s.rindex('.'))
    s = s.slice(0, n)
    next if approx_versions.include?(s)

    approx_versions.push s
    versions.push s
    print " #{s}"
  end
  if i == 1
    print ' latest'
    versions.push 'latest'
  end
  puts
  version_groups.push versions
end

FileUtils.mkdir_p(Output_d)

## - Generate the tags report for the public Docker registry:
TagsReport = Struct.new(:tags, :link_format)
report = TagsReport.new(version_groups, LINK_FORMAT)
template = ERB.new(File.read("#{__dir__}/tags.markdown.erb"), 0, '-')
f = "#{Output_d}/tags.markdown"
IO.write(f, template.result_with_hash(report: report))
info "Wrote tags report to: #{f}"
