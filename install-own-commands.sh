#!/bin/sh
d=$1
app_d=$2

mkdir -p "$d/bin"
for sh_f in lint-*.sh; do
    f=$d/bin/${sh_f%.sh};
    sed "s|^APP_HOME=.*$|APP_HOME=\"$app_d\"|g" "$sh_f" > "$f"
    chmod a+x "$f"
done
ls -l "$d/bin"/lint-*
