# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# frozen_string_literal: true

module Overcommit::Hook::PreCommit
  # Runs `YAMLLint` against any modified YAML files.
  #
  # @see https://github.com/adrienverge/yamllint
  class YamlLintRemade < Base
    MESSAGE_TYPE_CATEGORIZER = lambda do |type|
      type.include?('warning') ? :warning : :error
    end

    def run
      result = execute(command, args: applicable_files)
      return :pass if result.success?

      extract_messages(
        result.stdout.split("\n"),
        /^(?<file>(?:\w:)?[^:]+):(?<line>\d+):[^ ]+ (?<type>[^ ]+)/,
        MESSAGE_TYPE_CATEGORIZER
      )
    end
  end
end
