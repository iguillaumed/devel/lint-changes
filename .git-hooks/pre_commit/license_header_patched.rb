# frozen_string_literal: true

require 'overcommit/hook/pre_commit/license_header'

module Overcommit::Hook::PreCommit
  # Checks for license headers in source files
  class LicenseHeaderPatched < LicenseHeader
    def check_file(file, license_contents)
      j = 0
      File.readlines(file).each_with_index do |l, i|
        if j >= license_contents.length
          break
        end

        l.chomp!
        next if ignorable?(l, i, j)

        unless l.end_with?(license_contents[j])
          message = "#{file} missing header contents from line #{j + 1} of "\
                    "#{license_file}: #{license_contents[j]}"
          return message
        end
        j += 1
      end
    end

    def ignorable?(line, source_i, license_i)
      # allow the first line to be a shebang line:
      return true if source_i == 0 && line.start_with?('#!')

      # allow one empty line if we skipped the shebang before:
      return true if source_i == license_i + 1 && line.lstrip == ''

      false
    end
  end
end
