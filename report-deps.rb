#!/usr/bin/ruby
# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# frozen_string_literal: true

require 'csv'
require 'erb'
require 'fileutils'
require 'tempfile'
require 'open-uri'
require 'yaml'

include ERB::Util

# rubocop:disable Style/GlobalVars

CONF = YAML.load_file('report-deps.yml')
DISTRIB_CONF = CONF['distrib']

$deps_metadata = CONF['deps']

$license_text_files = DISTRIB_CONF['license_text_files']

$licenses_aliases = {
  'Apache 2.0' => 'Apache-2.0',
  'GPLv3' => 'GPL-3',
  'MIT' => 'MIT',
  'Mozilla Public License 2.0 (MPL 2.0)' => 'MPL-2.0',
  'ruby' => 'Ruby',
  'Simplified BSD' => 'BSD'
}

$licenses_metadata = {
  'GPL-3' => {
    'source_disclosure' => true
  },
  'MPL-2.0' => {
    'source_disclosure' => true
  }
}

$license_URLs = {
  'ruby' => 'http://www.ruby-lang.org/en/LICENSE.txt'
}

Output_d = $ARGV[0] || '.lint-changes/out/deps'

$licenses = {}
$copyrights = {}

def log(text)
  puts text if $DEBUG
end

def info(text)
  puts text if $VERBOSE
end

def notice(text)
  puts text
end

def error(text)
  $stderr.puts text
end

License = Struct.new(:name, :text, :path)

def file_extension_for_type(content_type)
  case content_type
  when 'text/html'
    '.html'
  else
    error "No known file extension for content-type #{content_type}!"
    exit
  end
end

def fetch_license(name, license_link)
  log "license_link: #{license_link}"
  uri = URI.parse(license_link)
  fh = uri.open
  media_type = fh.content_type
  text = fh.read
  return License.new(name, text, nil) if media_type == 'text/plain'

  license_file_path = "#{Output_d}/licenses/#{name}#{file_extension_for_type(media_type)}"
  FileUtils.mkdir_p(File.dirname(license_file_path))
  IO.write(license_file_path, text)
  License.new(name, '', license_file_path)
end

##
# Licenses whose text is not provided as file by the distribution.
#
class UncommonLicenses
  include Enumerable

  def initialize
    @no_canonical_text = 0
  end

  def each
    $licenses.each do |name, text|
      next if $license_text_files[name]

      if text != ''
        yield(License.new(name, text, nil))
        next
      end

      license_link = $license_URLs[name]
      if license_link
        begin
          text = fetch_license(name, license_link)
          yield text
          next
        rescue OpenURI::HTTPError => e
          error "Fetching the license failed: #{e.full_message}"
        end
      end

      error "No canonical license text for #{name}!"
      @no_canonical_text += 1
    end
  end

  def ok
    error "No canonical text: #{@no_canonical_text}" if @no_canonical_text > 0
    @no_canonical_text == 0
  end
end

def fetch_license_text(name)
  text = $licenses[name]
  return text if !text.nil?

  path = $license_text_files[name]
  return nil if path.nil?

  $licenses[name] = IO.read(path)
end

def remove_license_title_if_present(license_name, license, title_part_i = 0)
  parts = license.split("\n", 2)
  puts "PARTS: #{parts}" if title_part_i == 1
  title_part = parts[title_part_i]
  title_pattern = /^(The |)#{license_name} (License|LICENSE)/
  return license if title_part !~ title_pattern

  log "TITLE: #{title_part}"
  parts[1 - title_part_i] || ''
end

#FIXME: just for parser but does not work because newlines already replaced by spaces
def remove_license_title_if_present_at_the_end(license_name, license)
  remove_license_title_if_present(license_name, license, 1)
end

def remove_dep_stuff_if_present(dep_name, license)
  return license if !license.start_with?(dep_name)

  log "DEP-STUFF: #{license.split("\n", 2)[0]}"
  license.split("\n", 2)[1] || ''
end

def remove_frozen_string_literal_directive(license)
  return license if !license.start_with?('# frozen_string_literal: true')

  license.split("\n", 2)[1] || ''
end

def remove_copyright_if_present(license)
  return [license, nil] if !license.start_with?('Copyright')

  copyright, license = license.split("\n", 2)
  [license || '', copyright]
end

def dep_metadata(name, version)
  dep_metadata = $deps_metadata[name]

  return nil if !dep_metadata

  found_version = dep_metadata['version']
  return dep_metadata if found_version == version

  notice "Metadata available for #{name} #{found_version}, not #{version}..."

  nil
end

def compare_license_texts(text, license_name, dep_name, install_path)
  pristine_ref_text = fetch_license_text(license_name)
  return 0 if pristine_ref_text.nil? || pristine_ref_text.strip == ''

  text = text.gsub(/\s+/, ' ').strip
  ref_text = pristine_ref_text.gsub(/\s+/, ' ').strip
  if text.start_with?(ref_text)
    return 0 if ref_text.size == text.size

    residue = text.slice(ref_text.size, text.size - ref_text.size)
    warn "More than just the #{license_name} license for #{dep_name}:"
    warn "  #{residue}"
    info 'Assuming the additional text after it is copyright information.'
    $copyrights[dep_name] << "\n" << residue
    return 1
  elsif text.end_with?(ref_text)
    residue = text.slice(0, text.size - ref_text.size)
    residue = remove_license_title_if_present_at_the_end(license_name, residue)
    warn "More than just the #{license_name} license for #{dep_name}:"
    warn "  #{residue}"
    info 'Assuming the additional text before it is copyright information.'
    $copyrights[dep_name] << "\n" << residue
    return 1
  end
  error "Different #{license_name} license text for #{dep_name}!"
  license_text_f = Tempfile.new('license_')
  ref_license_text_f = Tempfile.new('ref_license_')
  license_text_f.write(text)
  ref_license_text_f.write(ref_text)
  log `diff -u #{license_text_f.path} #{ref_license_text_f.path}`
  log "See #{install_path}"
  -1
end

def load_file(install_path, filename)
  path = "#{install_path}/#{filename}"
  return nil if !File.exist?(path)

  File.read(path)
end

def handle_missing_copyrights(dep_name, dep_version, install_path)
  return if $copyrights[dep_name]

  info "No copyright text detected in license text for #{dep_name}."
  info "Looking for notice files in #{install_path}..."
  notice_text =
    load_file(install_path, 'NOTICE') ||
    load_file(install_path, 'NOTICE.txt') ||
    nil
  if !notice_text.nil?
    $copyrights[dep_name] = notice_text
    return
  end
  dep_metadata = dep_metadata(dep_name, dep_version)
  if dep_metadata && dep_metadata['copyrights']
    notice "Overriding copyrights for #{dep_name}!"
    $copyrights[dep_name] = dep_metadata['copyrights']
    return
  end
  info "Looking for copyright information in #{install_path}..."
  info '---8<---'
  info `cd  #{install_path} && grep -ri '^[^a-z]*\\s*copyright[: ][^a-z]' | awk -F':' '{first=$1;$1="";print first "  " $0}' | sort -k 2 | uniq -f 1 --group`
  info '--->8---'
end

def handle_license(original_text, license_name, dep_name, dep_version, install_path)
  dep_metadata = dep_metadata(dep_name, dep_version)
  if original_text.nil? || original_text.strip == ''
    info "No #{license_name} license text for #{dep_name}."
    $licenses[license_name] ||= ''
    if !dep_metadata || !dep_metadata['license_file']
      handle_missing_copyrights(dep_name, dep_version, install_path)
      return true
    end
  end
  if dep_metadata && dep_metadata['license_file']
    license_file = dep_metadata['license_file']
    original_text = File.read("#{install_path}/#{license_file}")
    notice "Overriding license file with #{license_file} for #{dep_name}."
  end
  text = original_text
  loop do
    text = text.lstrip
    previous_text = text
    text = remove_frozen_string_literal_directive(text)
    text = remove_license_title_if_present(license_name, text)
    text = remove_dep_stuff_if_present(dep_name, text)
    text, copyright = remove_copyright_if_present(text)
    $copyrights[dep_name] = copyright if copyright
    break if text == previous_text
  end
  handle_missing_copyrights(dep_name, dep_version, install_path)
  if !text.include?("\n")
    notice "No full #{license_name} license for #{dep_name}! #{text}"
    return false
  end
  result = compare_license_texts(text, license_name, dep_name, install_path)
  return false if result < 0
  return true if result > 0

  if dep_metadata && dep_metadata['canonical_license_text'] == false
    notice "Avoid using non-canonical #{license_name} license text from #{dep_name}."
    text = ''
  end
  $licenses[license_name] = text
  true
end

def set_license_link(license_name, license_link)
  return if !license_link || license_link == $license_URLs[license_name]

  warn "Different #{license_link} link for #{license_name} license!" if $license_URLs[license_name]
  $license_URLs[license_name] = license_link
end

Dep = Struct.new(:name, :version, :link, :license_identifier, :license_link, :install_path)

##
# Dependencies license_finder reported.
#
class Deps
  include Enumerable

  def initialize
    @not_approved = 0
    @not_supported = 0
    @total = 0
    @cached_deps = nil
  end

  def each
    if @cached_deps
      @cached_deps.each do |dep|
        yield dep
      end
      return
    end

    deps = []
    CSV.foreach('license_report.csv', {
      encoding: 'UTF-8',
      headers: true,
      field_size_limit: 1000
    }) do |line|
      dep = handle_line(line)
      next if !dep

      deps.push(dep)
      yield dep
    end
    @cached_deps = deps
  end

  def ok
    info "Total: #{@total}"
    error "Not supported: #{@not_supported}" if @not_supported > 0
    error "Not approved: #{@not_approved}" if @not_approved > 0
    #NOTE: Not failing on not approved deps because l_f reports seem to
    # only account for the license itself and not for manually approved deps:
    @not_supported == 0
  end

private

  def handle_line(line)
    @total += 1
    licenses = line['licenses']
    license_links = line['license_links']
    texts = line['texts'].gsub('\@NL', "\n")
    name = line['name']
    version = line['version']
    dep_metadata = dep_metadata(name, version)
    if dep_metadata && dep_metadata['licenses']
      licenses = dep_metadata['licenses']
      notice "Overriding license(s) with #{licenses} for #{name}."
      license_links = nil
      texts = nil
    end
    if line['approved'] != 'Approved'
      error "Not approved license(s) #{licenses} (#{license_links})!"
      @not_approved += 1
    end
    link = line['homepage']
    install_path = line['install_path']
    if install_path&.start_with?('/usr/') && !install_path.start_with?('/usr/local/')
      notice "The distribution itself provides #{name}, no need to redo the work."
      return nil
    end
    if dep_metadata && dep_metadata['relative_install_path']
      notice "Correct misdetected #{install_path} install path from #{name}."
      install_path = "#{install_path}/#{dep_metadata['relative_install_path']}"
    end
    license, license_link =
      merge_licenses(name, version, install_path, texts, licenses, license_links)
    return nil if license.nil?

    #NOTE: Keep for debugging: puts line
    license_identifier = $licenses_aliases[license]
    Dep.new(name, version, link, license_identifier, license_link, install_path)
  end
end

def merge_licenses(name, version, install_path, texts, licenses, license_links)
  best_license = nil
  best_license_link = nil
  i = 0
  license_list = licenses.split(',')
  #XXX HACK: just ignore multiple licenses' texts because
  # l_f reports offer no reliable way to split them (TODO?: file l_f bug?)
  text = license_list.length == 1 ? texts : ''
  license_list.each do |license|
    license_link = license_links ? license_links.split(',')[i] : nil
    i += 1
    set_license_link(license, license_link)
    if $licenses_aliases[license]
      return [nil, nil] if !handle_license(text, license, name, version, install_path)

      if better_license(license, best_license)
        best_license = license
        best_license_link =
          $license_text_files[license] || "##{url_encode(license)}"
      end
    else
      $licenses[license] = ''
      error "Unsupported license #{license} (#{license_link})!"
      license_link ||= "##{url_encode(license)}"
      @not_supported += 1
    end
  end
  if best_license.nil?
    error "Unsupported licenses #{licenses} (#{license_links})!"
    @not_supported += 1
  end
  [best_license, best_license_link]
end

def better_license(license, another_license)
  return true if another_license.nil?

  # The Ruby license is a dual license which includes Simplified BSD
  # so Simplified BSD is the simpler and weaker one:
  return true if license == 'Simplified BSD' && another_license == 'ruby'

  false
end

deps = Deps.new
uncommon_licenses = UncommonLicenses.new
FileUtils.mkdir_p(Output_d)

DepsReport = Struct.new(:deps, :licenses, :distrib_path)
distrib_path = DISTRIB_CONF['doc_path']
report = DepsReport.new(deps, uncommon_licenses, distrib_path)
template = ERB.new(File.read("#{__dir__}/deps.xhtml.erb"), 0, '><>-')
IO.write("#{Output_d}/deps.xhtml", template.result_with_hash(report: report))

SourcesReport = Struct.new(:deps, :licenses, :distrib_link)
distrib_link = format(
  DISTRIB_CONF['link_format'],
  DISTRIB_CONF['Git_repo_ref'],
  DISTRIB_CONF['Docker_image_tag']
)
report = SourcesReport.new(deps, $licenses_metadata, distrib_link)
template = ERB.new(File.read("#{__dir__}/sources.markdown.erb"), 0, '><>-')
IO.write("#{Output_d}/sources.markdown", template.result_with_hash(report: report))

info "Wrote deps report files to: #{Output_d}"

# rubocop:enable Style/GlobalVars

exit 1 if !deps.ok || !uncommon_licenses.ok
