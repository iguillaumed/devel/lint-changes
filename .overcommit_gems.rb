# frozen_string_literal: true

source 'http://rubygems.org'

gem 'overcommit'

#HACK: to avoid needing Ruby 2.6, although really it should be < 16.5.77,
# but 16.6.14 has been working fine so far for what is needed from it:
gem 'chef-utils', '< 16.7.61'

gem 'license_finder'
gem 'mdl'
gem 'rubocop'
gem 'yaml'
