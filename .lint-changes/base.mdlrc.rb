# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# frozen_string_literal: true

all

rule 'header-style', style: :setext_with_atx
# Cf. https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md#md003---header-style

# NOTE: Must allow bare URLs because reference-style links are misdetected
# as such (even when enclosed in angle brackets),
# see https://github.com/markdownlint/markdownlint/issues/191#issuecomment-694398023
exclude_rule 'no-bare-urls'
# Cf. https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md#md034---bare-url-used
