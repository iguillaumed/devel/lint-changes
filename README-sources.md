## Source code disclosure

The licenses of some of the software used require disclosing the source code.

All the software coming from the Linux distribution is documented at:
https://github.com/docker-library/repo-info/blob/63c8a9442690209b474b770e789f75c1d18d467a/repos/debian/local/buster-20220328-slim.md

For the rest, please see below.

### Additionally included software that require making the source code available on distribution

- hadolint 1.18.0-6-ga0d655d (GPL-3): [source code](https://hub.docker.com/r/hadolint/hadolint)
- pathspec 0.8.0 (MPL-2.0): [source code](https://github.com/cpburnz/python-path-specification)
- shellcheck 0.7.1 (GPL-3): [source code](https://hub.docker.com/r/koalaman/shellcheck)
- yamllint 1.25.0 (GPL-3): [source code](https://github.com/adrienverge/yamllint)
