#!/bin/sh

# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

create_if_missing() {
    f=$1
    text=$2

    [ -e "$f" ] || { echo "$text" > "$f"; return 0 ;}
    return 1
}

copy_if_missing() {
    f=$1
    d=$2

    [ -e "$d/$(basename "$f")" ] || { cp -pRi "$f" "$d"/; return 0 ;}
    return 1
}

begin_collapsed_section() {
    output_unique_ID=$1
    title=$2

    if [ -n "$COLLAPSED_SECTION_PRELUDE_FORMAT" ]; then
        # shellcheck disable=SC2059
        printf "$COLLAPSED_SECTION_PRELUDE_FORMAT" \
         "$(date +%s)" "$output_unique_ID" "$title"
    fi
}

end_section() {
    output_unique_ID=$1

    if [ -n "$SECTION_POSTLUDE_FORMAT" ]; then
        # shellcheck disable=SC2059
        printf "$SECTION_POSTLUDE_FORMAT" \
         "$(date +%s)" "$output_unique_ID"
    fi
}

create_fallback_config_if_missing() {
    config_default_filename=$1
    min_config_text_format=$2
    base_suffix=${3:-$config_default_filename}

    cp --update -p "/app/base$base_suffix" ./.lint-changes
    # shellcheck disable=SC2059
    create_if_missing "./$config_default_filename" \
     "$(printf "$min_config_text_format" ".lint-changes/base$base_suffix")"
}

case $APP_HOME in
/app)
    # GitLab already changes to the project's Git working tree directory
    # (also it does not have a fixed name!):
    [ -n "$CI_PROJECT_DIR" ] || cd /repo || exit 2

    case $(whoami 2>/dev/null) in
    nobody)
        # GitLab seems to grant all users the owner's permissions for all files
        # => in that case even the 'nobody' user can be used:
        if [ -z "$CI_PROJECT_DIR" ]; then
            cat <<EOUserError
No user defined!
Please run the container using "docker run -u \`id -u\`:\`id -g\`"
instead of just "docker run"
(for other container runtimes please use the corresponding options).
EOUserError
            exit 2
        fi;;
    *)
    esac

    begin_collapsed_section 'lint-changes-init-overcommit' 'Set up overcommit'

    re_sign_pre_commit=false
    mkdir -p ./.git-hooks/pre_commit
    copy_if_missing /app/pre_commit/license_header_patched.rb \
     ./.git-hooks/pre_commit && \
        re_sign_pre_commit=true
    copy_if_missing /app/pre_commit/yaml_lint_remade.rb \
     ./.git-hooks/pre_commit && \
        re_sign_pre_commit=true
    mkdir -p ./.lint-changes

    create_fallback_config_if_missing .mdlrc \
     'style "#{File.dirname(__FILE__)}/%s"' .mdlrc.rb

    create_fallback_config_if_missing .rubocop.yml \
     'inherit_from: %s'

    create_fallback_config_if_missing .yamllint.yml \
     'extends: %s'

    # Sign automatically only on first use, and otherwise
    # let the user sign its own modifications:
    if ! git config --show-origin \
     --get overcommit.configuration.verifysignatures; then
        overcommit --sign
        overcommit --sign pre-commit
    fi

    # Sign only our own custom hooks automatically, only when needed:
    $re_sign_pre_commit && overcommit --sign pre-commit

    end_section 'lint-changes-init-overcommit';;
*)
    echo "APP_HOME: $APP_HOME"
esac
