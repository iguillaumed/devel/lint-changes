#!/usr/bin/ruby
# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# frozen_string_literal: true

require 'overcommit'

def info(text)
  puts text if $VERBOSE
end

# Allows converting the internal hash back to YAML.
class WritableOvercommitConfiguration < Overcommit::Configuration
  def initialize(hash, options = {})
    super(hash, {
      validate: false,
      logger: options[:logger]
    })
  end

  def baseline(new_one)
    #HACK: [...over below hack...] only handling PreCommit hooks for now:
    #HACK: add the 'ALL' parts manually to reduce the diff:
    baseline = merge(self.class.new({
      'PreCommit' => {
        'ALL' => new_one.hash['PreCommit']['ALL']
      }
    }, @options))
    # ...and patch backwards to keep the differences at only one place:
    baseline.hash['PreCommit']['ALL'] = @hash['PreCommit']['ALL']
    baseline
  end

  def to_yaml
    @hash.to_yaml
  end
end

def default_f
  Overcommit::ConfigurationLoader::DEFAULT_CONFIG_PATH
end

DIFF_ONLY = ARGV.length != 1 || ARGV[0] != 'merge-and-replace'

def replace_default(overlay_for_default_f)
  unwritable_default =
    Overcommit::ConfigurationLoader.default_configuration
  info "Read default configuration from: #{default_f}"

  unwritable_new_default =
    Overcommit::ConfigurationLoader.load_from_file(overlay_for_default_f)
  info "Read new default configuration overlay from: #{overlay_for_default_f}"

  config_options = { logger: Overcommit::Logger.new($stderr) }
  null_one = WritableOvercommitConfiguration.new({}, config_options)
  default = null_one.merge(unwritable_default)

  write_new_default(default.merge(unwritable_new_default))
  return if !DIFF_ONLY

  baseline_default_f = 'a'
  IO.write(baseline_default_f, default.baseline(unwritable_new_default).to_yaml)
  info "Wrote baseline default configuration to: #{baseline_default_f}"
end

def write_new_default(new_default)
  backup_f = "#{default_f}.orig-lint-changes"
  if !DIFF_ONLY && !File.file?(backup_f)
    FileUtils.copy(default_f, backup_f)
    info "Becked up default configuration to: #{backup_f}"
  end
  new_default_f = DIFF_ONLY ? 'b' : default_f
  IO.write(new_default_f, new_default.to_yaml)
  info "Wrote new default configuration to: #{new_default_f}"
end

replace_default('overlay-for-default.overcommit.yml')
