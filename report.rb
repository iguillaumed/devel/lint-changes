#!/usr/bin/ruby
# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# frozen_string_literal: true

require 'date'
require 'digest'
require 'erb'
require 'fileutils'
require 'json'

SECTION_PRELUDE_FORMAT = ENV['SECTION_PRELUDE_FORMAT']
COLLAPSED_SECTION_PRELUDE_FORMAT = ENV['COLLAPSED_SECTION_PRELUDE_FORMAT']
SECTION_POSTLUDE_FORMAT = ENV['SECTION_POSTLUDE_FORMAT']
REPORT_TYPES = ENV['REPORT_TYPES']

include ERB::Util

# rubocop:disable Style/GlobalVars

Output_d = '.lint-changes/out'

def log(text)
  puts text if $DEBUG
end

def info(text)
  puts text if $VERBOSE
end

def warn(text)
  puts text
end

def error(text)
  $stderr.puts text # rubocop:disable Style/StderrPuts
end

timestamp = Time.now.utc.strftime('%Y-%m-%dT%H:%M:%S')

SuiteStats = Struct.new(
  :total,
  :disabled,
  :errors,
  :failures,
  :skipped,
  :timestamp
)
Suite = Struct.new(:stats, :cases)
Case = Struct.new(
  :status,
  :id,
  :message,
  :description,
  :time,
  :assertions
)
Assertion = Struct.new(:file, :text, :line, :column, :severity)

FailedAssertionKind = Struct.new(
  :severity,
  :group_count,
  :qualifier,
  :collapsed
)

$failed_assertion_kinds = {
  'failed_assertions' => FailedAssertionKind.new(
    'blocker',
    0,
    'FAILED',
    false
  ),
  'ignored_failed_assertions' => FailedAssertionKind.new(
    'major',
    0,
    'ignored failed',
    true
  ),
  'ignored_unchanged_failed_assertions' => FailedAssertionKind.new(
    'minor',
    0,
    'unchanged ignored failed',
    true
  ),
  'unchanged_failed_assertions' => FailedAssertionKind.new(
    'critical',
    0,
    'unchanged FAILED',
    true
  )
}

SKIPPED_PREFIX = 'Skipping '

$state = nil
$suite = Suite.new(SuiteStats.new(0, 0, 0, 0, 0, timestamp), [])
$case = nil

def without_ANSI_escape_sequences(text)
  text.gsub(/\e\[?.*?[@-~]/, '')
  # ...converted from Perl-based `perl -pe 's/\e\[?.*?[\@-~]//g'` found at
  # https://docs.gitlab.com/ee/ci/pipelines/settings.html#removing-color-codes
end

def ignore_line(line)
  return if line.strip != 'Running pre-commit hooks'

  $state = 'summary'
  $start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
end

def split_summary_line(line)
  line.scrub!

  message, id, result = line.split(/[\[\]]/)

  return [''.dup, line.partition(SKIPPED_PREFIX).last, ']SKIPPED['.dup] \
    if id.nil? && line.start_with?(SKIPPED_PREFIX)

  return [nil, nil, nil] if message.nil? || !message.end_with?('..')

  [message, id, result]
end

def handle_summary_line(line)
  message, id, result = split_summary_line line
  if id.nil?
    $state = 'details'
    return handle_details_line line
  end
  end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
  time = end_time - $start_time
  $start_time = end_time
  $suite.stats.total += 1
  message.strip!
  id.strip!
  result.strip!
  case result
  when 'OK'
    nil
  when 'FAILED'
    $suite.stats.failures += 1
  when 'WARNING'
    $suite.stats.disabled += 1
  when ']SKIPPED['
    result = 'skipped'
    $suite.stats.skipped += 1
    $state = 'summary'
  else
    $suite.stats.errors += 1
  end
  $case = Case.new(result, id, message, ''.dup, time, [])
  $suite.cases << $case
  true
end

def new_failed_assertions_state(line)
  new_state = nil
  if line.end_with?('on modified lines:')
    new_state = 'failed_assertions'
  elsif line.end_with?('on lines you didn\'t modify:')
    new_state = 'unchanged_failed_assertions'
  else
    return nil
  end

  if line.start_with?('Errors ')
    new_state
  elsif line.start_with?('Warnings ')
    "ignored_#{new_state}"
  else
    nil
  end
end

def begin_section(line)
  section_prelude_format = $failed_assertion_kinds[$state].collapsed ?
    COLLAPSED_SECTION_PRELUDE_FORMAT :
    SECTION_PRELUDE_FORMAT
  return false if !section_prelude_format

  prelude = format(
    section_prelude_format,
    Time.now.utc.to_i,
    "#{$case.id}-#{$state}-lint-changes-report",
    line
  )
  $stdout.print prelude if !$VERBOSE.nil?
  true
end

def handle_details_line(line)
  _, id = split_summary_line line
  if !id.nil?
    $state = 'summary'
    handle_summary_line line
    return true
  end
  $case.description << line << "\n"

  new_state = new_failed_assertions_state(line)
  if !new_state.nil?
    $state = new_state

    return false if begin_section(line)
  end
  true
end

def split_failed_assertion_line(line)
  file, rest = line.split(':', 2)

  return [nil, nil, nil, nil] if rest.nil?

  line_number, column_number_and_or_text = rest.split(':', 2)

  if column_number_and_or_text.nil?
    line_number, text = rest.split(/[ ^0-9]/, 2)
    return [nil, nil, nil, nil] if text.nil?

    return [file, line_number, nil, text]
  end

  column_number, text = column_number_and_or_text.split(':', 2)

  if text.nil?
    text = column_number
    column_number = nil
  end

  [file, line_number, column_number, text]
end

def end_section
  return if !SECTION_POSTLUDE_FORMAT

  postlude = format(
    SECTION_POSTLUDE_FORMAT,
    Time.now.utc.to_i,
    "#{$case.id}-#{$state}-lint-changes-report"
  )
  $stdout.print postlude if !$VERBOSE.nil?
end

def output_group_total(failed_assertion_kind)
  qualifier = failed_assertion_kind.qualifier
  group_count = failed_assertion_kind.group_count
  warn "Total: #{group_count} #{qualifier} #{$case.id} ASSERTIONS."
end

def handle_line(line)
  file, line_number, column_number, text = split_failed_assertion_line(line)
  if line_number.nil?
    end_section
    new_state = new_failed_assertions_state(line)
    failed_assertion_kind = $failed_assertion_kinds[$state]
    output_group_total(failed_assertion_kind)
    failed_assertion_kind.group_count = 0
    if !new_state.nil?
      $case.description << line << "\n"
      $state = new_state
      return !begin_section(line)
    end
    $state = 'summary'
    return handle_summary_line line
  end
  $case.description << line << "\n"
  failed_assertion_kind = $failed_assertion_kinds[$state]
  severity = failed_assertion_kind.severity
  assertion = Assertion.new(file, text, line_number, column_number, severity)
  $case.assertions << assertion
  failed_assertion_kind.group_count += 1
  true
end

while (full_line = gets)
  line = full_line.chomp

  must_output_line = true

  case $state
  when 'summary'
    must_output_line = handle_summary_line line
  when 'details'
    must_output_line = handle_details_line line
  when 'failed_assertions'
    must_output_line = handle_line line
  when 'ignored_failed_assertions'
    must_output_line = handle_line line
  when 'ignored_unchanged_failed_assertions'
    must_output_line = handle_line line
  when 'unchanged_failed_assertions'
    must_output_line = handle_line line
  else
    ignore_line line
  end

  next if !must_output_line

  $stdout.print full_line if !$VERBOSE.nil?
  $stdout.flush if !$VERBOSE.nil?
end

end_section if $state.end_with?('failed_assertions')
output_group_total($failed_assertion_kinds[$state]) if $state.end_with?('failed_assertions')

def dump_model
  info $suite.stats
  $suite.cases.each do |kase|
    info "   #{kase.id} #{kase.status} #{format('%.9f', kase.time)}"
    log kase.message
  end
end

def code_quality_CodeClimate_report
  cqr = []
  $suite.cases.each do |kase|
    kase.assertions.each do |assertion|
      fingerprint = Digest::MD5.hexdigest(assertion.to_s)
      cqr << {
        description: "#{assertion.text} [0x#{fingerprint}]",
        fingerprint: fingerprint,
        severity: assertion.severity,
        location: {
          path: assertion.file,
          lines: {
            begin: assertion.line
          }
        }
      }
    end
  end
  cqr
end

dump_model if $VERBOSE

def write_JUnit_report
  template = ERB.new(File.read("#{__dir__}/junit.xml.erb"), 0, '><>-')
  f = "#{Output_d}/overcommit.junit.xml"
  IO.write(f, template.result_with_hash(suite: $suite))
  # GitLab can show it as artifact just as well => unneeded: log File.read(f)
  info "Wrote JUnit report to: #{f}"
end

def write_CodeClimate_report
  f = "#{Output_d}/codequality.codeclimate.json"
  IO.write(f, code_quality_CodeClimate_report.to_json)
  # GitLab can show it as artifact just as well => unneeded: log File.read(f)
  info "Wrote CodeClimate report to: #{f}"
end

FileUtils.mkdir_p(Output_d)

case REPORT_TYPES
when 'NONE'
  info 'Generated no reports as asked.'
when 'JUnit'
  write_JUnit_report
when 'CodeClimate'
  write_CodeClimate_report
when 'ALL'
  write_JUnit_report
  write_CodeClimate_report
else
  warn "Missing or invalid REPORT_TYPES environment variable value.\n"\
    'Please set it to NONE, JUnit. CodeClimate, or ALL.'
end

# rubocop:enable Style/GlobalVars
