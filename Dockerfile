# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

ARG hadolint_Docker_image_version=v1.18.0-6-ga0d655d
ARG shellcheck_Docker_image_version=v0.7.1
ARG Debian_Docker_image_version=buster-20220328-slim

#NOTE: The following does not work because hadolint does not support
# ARGs well, but maybe more granularity is better anyway:
#ARG docker_deps_frozen_if_not_null=foo
#FROM [...]${docker_deps_frozen_if_not_null:+:v[...]} AS [...]

FROM hadolint/hadolint:$hadolint_Docker_image_version \
 AS hadolint-augmented-scratch

FROM koalaman/shellcheck:$shellcheck_Docker_image_version \
 AS shellcheck-augmented-scratch

FROM busybox:1-musl AS context-mods
COPY install-own-commands.sh \
 lint-*.sh ./
RUN sh install-own-commands.sh /. /app

FROM debian:${Debian_Docker_image_version}
#FROM ruby:slim-buster
#FROM ruby:2.6-slim-buster
#FROM ruby:2.4
WORKDIR /app
ARG python_deps_frozen_if_not_null=foo
COPY install-Debian-packages.sh requirements.txt \
 requirements${python_deps_frozen_if_not_null:+-lock}.txt ./
ARG DEBIAN_FRONTEND=noninteractive
# Just right, seems to show all questions even with the noninteractive frontend:
ARG DEBCONF_DEBUG="developer"
# Too noisy:
#ARG DEBCONF_DEBUG=".*"
#NOTE: The cryptic debconf stuff below prevents its databases
# to be overwritten which caused >2MB image layer waste (see
# https://github.com/debuerreotype/debuerreotype/issues/95#issuecomment-699659904
# hadolint ignore=SC1091,SC2086
RUN { sed \
     -e '/^Config: / s/configdb/configdb1/' \
     -e '/Templates: / s/templatedb/templatedb1/' \
     /etc/debconf.conf | \
    awk '/^Mode: 644/ { in_FD=1 } \
     /^$/ { if (in_FD) { print "Readonly: true" }; in_FD=0 } \
     ! /^#.*$/ { print $0 }'; \
    echo "\n\
Name: config1\n\
Driver: PackageDir\n\
Mode: 644\n\
Reject-Type: password\n\
Directory: /var/cache/debconf/config1.d\n\
Backup: false\n\
\n\
Name: configdb1\n\
Driver: Stack\n\
Stack: config1, config, passwords\n\
\n\
Name: templates1\n\
Driver: PackageDir\n\
Mode: 644\n\
Directory: /var/cache/debconf/templates1.d\n\
Backup: false\n\
\n\
Name: templatedb1\n\
Driver: Stack\n\
Stack: templates1, templatedb\n\
\n" ;} > ~/.debconfrc && \
    . ./install-Debian-packages.sh && \
    python3 -V && \
    if ! [ -e requirements-lock.txt ]; then \
        pip3 --no-cache-dir install -r requirements.txt && \
        pip3 --no-cache-dir freeze -r requirements.txt > requirements-lock.txt; \
    else \
        pip3 --no-cache-dir install -r requirements-lock.txt; \
    fi && \
    cat requirements-lock.txt && \
    apt-get purge -y --autoremove $LINTING_DEVEL_DEBIAN_PACKAGES && \
    rm -rf /var/lib/apt/lists/*
#NOTE: the above relative path for `.` confuses shellcheck ATM,
# see https://github.com/koalaman/shellcheck/issues/1818
# so SC1091 had to be worked around somehow (deactivated!)
ARG overcommit_deps_frozen_if_not_null=foo
COPY .overcommit_gems.rb \
 .overcommit_gems.rb${overcommit_deps_frozen_if_not_null:+.lock} \
 deps.xhtml.erb \
 overcommit-config.rb \
 overlay-for-default.overcommit.yml \
 report-deps.rb \
 report-deps.yml \
 set-up-overcommit.sh \
 sources.markdown.erb \
 config/ doc/ .git-hooks/ \
 ./
RUN sh set-up-overcommit.sh && rm -Rf .git/ && \
    : Delete unneeded Bundler caches: ; \
    #HACK: bundle install --no-cache does not work, see
    # https://github.com/rubygems/rubygems/issues/3225:
    rm -Rf bundle/ruby/2.*.*/cache/ && \
    rm -Rf ~/.bundle/cache/ && \
    : Delete unneeded Gem cache: ; \
    rm -Rf /var/lib/gems/2.*.*/cache/ && \
    : Delete unneeded Gem spec cache: ; \
    rm -Rf ~/.gem/specs && \
    rmdir ~/.gem && \
    #HACK: bootstrap bundler: ; \
    RUBYLIB_FOR_BUNDLER=$(find /var/lib/gems/2.5.0/gems/ -name "bundler-*")/lib && \
    echo \
     '$:.unshift File.expand_path("'"$RUBYLIB_FOR_BUNDLER"'")' \
     >> /app/bundle/bundler/setup.rb && \
    # Reset bundler environment to make sure nothing leaks
    # (this also documents how to run license_finder manually, see below):
    grep -H ^ .bundle/config && \
    rm .bundle/config && \
    rmdir .bundle && \
    rmdir ~/.bundle && \
    \
    : Generate licensing info for all dependencies: ; \
    mkdir -p config/ && \
    mv -f license_finder.yml config/ && \
    mkdir -p .bundle && \
    bundle config --local path bundle && \
    BUNDLE_GEMFILE=.overcommit_gems.rb license_finder report \
     --decisions-file=dependency_decisions.yml \
     --recursive --format csv --write-headers --columns \
        name \
        version \
        authors \
        licenses \
        license_links \
        approved \
        summary \
        homepage \
        install_path \
        package_manager \
        groups \
        texts \
        notice \
     --save license_report.csv && \
    mv config/license_finder.yml . && \
    rmdir config && \
    grep -H ^ .bundle/config && \
    rm .bundle/config && \
    rmdir .bundle && \
    ruby -W2 report-deps.rb / && \
    rm license_report.csv
#NOTE: The config/ hoops above are needed because license_finder.yml must
# be under ./doc/ (see https://github.com/pivotal/LicenseFinder/issues/131)
# and COPY flattens everything... :-/

COPY --from=hadolint-augmented-scratch /bin/hadolint /bin/
COPY --from=shellcheck-augmented-scratch /bin/shellcheck /bin/

COPY \
 .lint-changes/base.* \
 index.xhtml \
 init.sh \
 junit.xml.erb \
 report.rb \
 /app/
COPY --from=context-mods /bin/lint-* /bin/

USER nobody

#NOTE: cannot split LABEL lines on '=' because hadolint breaks on that,
# see https://github.com/hadolint/hadolint/issues/483:
LABEL \
    org.opencontainers.image.authors="Guillaume Déflache" \
    org.opencontainers.image.description="Lint only what changed in a repository in e.g. merge requests." \
    org.opencontainers.image.title="lint-changes"
#TODO: LABEL org.opencontainers.image.licenses SPDX-LICENSE-EXPRESSION
# see https://spdx.org/spdx-specification-21-web-version#h.jxpfx0ykyb60
#NOTE: other labels from https://github.com/opencontainers/image-spec/blob/master/annotations.md
# are defined elsewhere:
# - created, revision, source:
#   when building the image
# - documentation, url, vendor, version
#   when publishing the image

ENTRYPOINT ["/bin/bash"]
# hadolint seems to be confused by line breaks here:
# hadolint ignore=DL3025
CMD ["-c", "[ -t 0 ] || { \
 echo 'Please run the container using \"docker run -it\"\
 instead of just \"docker run\"'; \
 echo '(for other container runtimes please use the corresponding options).'; \
 exit 2; }; \
 echo; \
 echo 'Please press a key to continue:'; \
 echo; \
 echo 'A text-based web browser (\"w3m\") will start,'; \
 echo 'allowing you to consult licensing, copyright and other information.'; \
 read -rsn1 -t5; \
 if [ $? -ne 0 ]; then \
 echo; \
 echo; \
 echo 'If the key pressed has no effect or only gets displayed please'; \
 echo 'first hit Ctrl-C to stop'; \
 echo 'then re-run the container using \"docker run -it\"\
 instead of just \"docker run -t\"'; \
 echo '(for other container runtimes please use the corresponding options).'; \
 read -rsn1; \
 fi; \
 w3m \
 -o display_charset=UTF-8 \
 -o mark_all_pages=true \
 -o confirm_qq=false \
 /app/index.xhtml"]
