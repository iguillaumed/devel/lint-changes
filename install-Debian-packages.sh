#!/bin/sh

# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

LINTING_DEBIAN_PACKAGES="$LINTING_DEBIAN_PACKAGES \
 "

# Python:
LINTING_DEVEL_DEBIAN_PACKAGES="$LINTING_DEVEL_DEBIAN_PACKAGES \
 "
# python-setuptools needed by yamllint
LINTING_DEBIAN_PACKAGES="$LINTING_DEBIAN_PACKAGES \
 python3-pip \
 python3-setuptools \
 python3"
# python3-pip needed by license_finder (LicenseFinder::Pip)
# python3-setuptools needed by license_finder (LicenseFinder::Pip)

# Ruby:
LINTING_DEVEL_DEBIAN_PACKAGES="$LINTING_DEVEL_DEBIAN_PACKAGES \
 build-essential \
 ruby-dev"
LINTING_DEBIAN_PACKAGES="$LINTING_DEBIAN_PACKAGES \
 git \
 procps \
 ruby"


LINTING_DEBIAN_PACKAGES="$LINTING_DEBIAN_PACKAGES w3m \
 $LINTING_DEVEL_DEBIAN_PACKAGES"

# shellcheck disable=SC2086
apt-get update && apt-get install -y \
 --no-install-recommends $LINTING_DEBIAN_PACKAGES

export LINTING_DEVEL_DEBIAN_PACKAGES
