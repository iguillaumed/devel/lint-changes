#!/bin/sh

# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

commit_message=$1
#FIXME: use current branch as default instead:
target_branch=${2:-master}
branch=${3:-$target_branch}

APP_HOME="$(dirname "$0")"
# shellcheck source=./init.sh
. "$APP_HOME/init.sh"

begin_collapsed_section 'lint-changes-init-git' 'Set up git'

#export GIT_CONFIG_COUNT=1
#export GIT_CONFIG_KEY_1=core.hooksPath
#export GIT_CONFIG_VALUE_1=$APP_HOME/git_hooks
export GIT_CONFIG_PARAMETERS="'core.hooksPath="$APP_HOME"/git_hooks'"

# Really needed for CI where each clone might be redone from scratch:
overcommit --sign
overcommit --sign pre-commit

export RUBYOPT="-r $APP_HOME/bundle/bundler/setup $RUBYOPT"

git branch -vva
git log --format=oneline | head || true
cat .git/ORIG_HEAD || true
recommit_branch=recommit/$branch
git checkout -B "$recommit_branch"

git reset --soft "origin/$target_branch"
cat .git/ORIG_HEAD || true
cat .git/HEAD || true
git status || true
echo "Git commit message:"
echo "$commit_message"
prev_head=$(git log -n1 --format=format:"%H")
echo "$prev_head"

end_section 'lint-changes-init-git'

git \
 -c user.email="john.doe@example.com" \
 -c user.name="John Doe" \
 commit -m "$commit_message" \
 2>&1 | ruby -W2 "$APP_HOME/report.rb"

begin_collapsed_section 'lint-changes-reset-git' 'Tear down git'

cat .git/ORIG_HEAD || true
head=$(git log -n1 --format=format:"%H")

git checkout -
git branch -D "$recommit_branch"

end_section 'lint-changes-reset-git'

echo "$head"
# no commit was created: some Git hook(s) failed:
case $head in
"$prev_head") exit 1
esac
