#!/usr/bin/ruby
# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# frozen_string_literal: true

require 'yaml'

COMMIT_REF_NAME = ARGV[0]
PROJECT_RAW_FILE_URL_PREFIX = ARGV[1]

expected_tag_name = COMMIT_REF_NAME.delete_prefix('V')

if expected_tag_name == COMMIT_REF_NAME
  puts 'Not a version tag => exiting.'
  exit 0
end

DEPS_YAML = YAML.load_file('report-deps.yml')
documented_distrib_tag = DEPS_YAML['distrib']['Docker_image_tag']
distrib_tag_regexp = /^ARG Debian_Docker_image_version=/
actual_distrib_tag =
  IO.foreach('Dockerfile').grep(distrib_tag_regexp).first.split('=')[1].strip
if actual_distrib_tag != documented_distrib_tag
  warn "The documented distribution version #{documented_distrib_tag}\n" \
   "is not the actually used distribution version #{actual_distrib_tag}!"
  exit 1
end

CI_YAML = YAML.load_file('lib-pinned.gitlab-ci.yml')

actual_tag_name =
  CI_YAML['.init-linting-Docker-image']['variables']['LINTING_DOCKER_IMAGE_VERSION']

if actual_tag_name != expected_tag_name
  warn 'This pinned GitLab CI file must use the exact version'
  warn "#{expected_tag_name} of the Docker image: found"
  warn "#{actual_tag_name}!"
  exit 1
end

lib_link = "#{PROJECT_RAW_FILE_URL_PREFIX}lib.gitlab-ci.yml"
if !CI_YAML['include'].include?(lib_link)
  warn "This pinned GitLab CI file must include #{lib_link}!"
  exit 1
end
