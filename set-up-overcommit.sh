#!/bin/sh

# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

ruby -v

overcommit_gemfile_fn=.overcommit_gems.rb

## - Set up Bundler:
bundler_version_gem_install_option=
if [ -e $overcommit_gemfile_fn.lock ]; then
    bundler_version=$(awk '/^BUNDLED WITH/{b=1;next} /^/{if(b){print $1;b=0}}' \
     < $overcommit_gemfile_fn.lock)
    bundler_version_gem_install_option=" -v $bundler_version"
fi
# shellcheck disable=SC2086
gem install bundler -N $bundler_version_gem_install_option
bundler -v

## - Set up overcommit:
bundle install -j "$(nproc)" \
 --gemfile="$overcommit_gemfile_fn" \
 --standalone \
 --no-cache
cat "$overcommit_gemfile_fn.lock"
bundle config --local gemfile "$overcommit_gemfile_fn"
bundle binstubs \
 license_finder \
 mdl \
 overcommit \
 rubocop \
 --path /usr/local/bin --standalone
git init

export GIT_CONFIG_PARAMETERS="'core.hooksPath="$PWD"/git_hooks'"
overcommit --install

bundle exec ruby -w overcommit-config.rb
git diff --no-index --color a b
rm a b
bundle exec ruby -w overcommit-config.rb merge-and-replace
