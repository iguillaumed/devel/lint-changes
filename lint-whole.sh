#!/bin/sh

# Copyright Contributors to the lint-changes project.
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

APP_HOME="$(dirname "$0")"
# shellcheck source=./init.sh
. "$APP_HOME/init.sh"
p=.lint-changes/out/.overcommit.pipe
mkdir -p "$(dirname "$p")"
mkfifo "$p"
ruby -W2 "$APP_HOME/report.rb" < "$p" &
pid=$!
SKIP=AuthorEmail,AuthorName overcommit --run \
 > "$p" 2>&1
exit_code=$?
wait $pid
exit $exit_code
